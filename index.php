<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Yellowtail&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Fraunces:wght@400;500;600;700;800;900&display=swap" rel="stylesheet">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="assets/vendor/lineIcons/css/LineIcons.css">
    <link rel="stylesheet" href="assets/vendor/slick/slick.css">
    <!-- <link rel="stylesheet" href="assets/vendor/slick/slick-theme.css"> -->

    <!-- Theme CSS -->
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/default.css">
    <link rel="stylesheet" href="assets/css/common/themecolor/theme-color.green.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/common/toolbar/toolbar-default.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <title>Hello, world!</title>
</head>

<body>
    <div class="website-wrapper">
        <!-- HEADER START-->
            <header classs="header">
                <div class="main-header">
                    <!--TOP BAR-->
                    <?php include('common/toolbar/topbar-three.php') ?>
                    
                    <!--GENERAL HEADER-->


                </div>
            </header>
        <!-- HEADER END-->
    </div>
    <!-- Bootstrap JS -->
    <script src="assets/js/jquery-3.6.0.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Vendor JS -->
    <script src="assets/vendor/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
</body>

</html>