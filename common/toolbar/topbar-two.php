<div class="topbar">
    <div class="container pr-64 pl-64">
        <div class="row">
            <div class="col-lg-6 d-none d-lg-block">
                <div class="topbar-left">
                    <div class="topbar-divider d-none d-lg-block"></div>
                    <div class="topbar-dropdown d-none d-lg-block">
                        <ul>
                            <li>
                                <a href="#">Langauge</a>
                                <div class="topbar-dropdown-sub topbar-dropdown-menu">
                                    <ul>
                                        <li>
                                            <a href="#">English</a>
                                        </li>
                                        <li>
                                            <a href="#">Bangla</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="#">Currency</a>
                                <div class="topbar-dropdown-sub topbar-dropdown-menu">
                                    <ul>
                                        <li>
                                            <a href="#">USD</a>
                                        </li>
                                        <li>
                                            <a href="#">PHP</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="topbar-phone">
                        HELP LINE: 
                        <span>
                            <a href="#">(88) 1900 123 456</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="topbar-right">
                    <div class="topbar-social">
                        <a href="#">
                            <i class="lni lni-facebook-filled"></i>
                        </a>
                        <a href="#">
                            <i class="lni lni-twitter-filled"></i>
                        </a>
                        <a href="#">
                            <i class="lni lni-instagram"></i>
                        </a>
                    </div>
                    <div class="topbar-divider d-none d-lg-block"></div>
                    <div class="topbar-links-section d-none d-lg-block">
                        <a href="">Contact Us</a>
                    </div>
                    <div class="topbar-divider d-none d-lg-block"></div>
                    <div class="topbar-links-section d-none d-lg-block">
                        <a href="">Faq</a>
                    </div>
                    <div class="topbar-divider d-none d-lg-block"></div>
                </div>
            </div>
        </div>
    </div>
</div>